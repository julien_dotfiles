call plug#begin('~/.local/share/nvim/plugged')
Plug 'Shougo/deoplete.nvim'
Plug 'zchee/deoplete-clang'
Plug 'sebastianmarkow/deoplete-rust'
Plug 'rust-lang/rust.vim'
Plug 'weakish/rcshell.vim'
"Plug 'clojure-vim/async-clj-omni'
call plug#end()

" Security
set modelines=0

" Show line numbers
set number

" Show file stats
set ruler

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Whitespace
set wrap
set textwidth=79
set formatoptions=tcqrn1
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set noshiftround

" Allow hidden buffers
set hidden

" Rendering
set ttyfast


set mouse=a


" Color scheme (terminal)
set t_Co=256
colorscheme eva01

if has('nvim')
    "call deoplete#enable()
    let g:deoplete#sources#clang#libclang_path = "/usr/lib64/libclang.so"
    let g:deoplete#sources#clang#clang_header = "/usr/lib64/clang/6.0.0/include"
    let g:deoplete#sources#clang#std = {'c': 'c99', 'cpp': 'c++1z', 'objc': 'c11', 'objcpp': 'c++1z'}
    let g:deoplete#sources#clang#flags = [ "-Wall", "-Wextra", "-Wconversion", "-pedantic" ]
    "let g:deoplete#keyword_patterns = {}
    "let g:deoplete#keyword_patterns.clojure = '[\w!$%&*+/:<=>?@\^_~\-\.#]*'
    let g:deoplete#enable_at_startup = 1
    ":call deoplete#custom#set('async_clj', 'debug_enabled', 1)
    ":call deoplete#enable_logging("DEBUG", "/tmp/deopletelog")
    let g:deoplete#sources#rust#racer_binary='/home/nelker/.cargo/bin/racer'
    let g:deoplete#sources#rust#rust_source_path="$RUST_SRC_PATH"
end
