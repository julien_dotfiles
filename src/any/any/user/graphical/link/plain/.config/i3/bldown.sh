#!/bin/sh
bl="$(cat /sys/class/backlight/intel_backlight/brightness)"
new_bl=$((bl - 20))
echo "$new_bl" > /sys/class/backlight/intel_backlight/brightness
