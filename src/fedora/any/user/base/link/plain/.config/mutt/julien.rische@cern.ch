set from = "julien.rische@cern.ch"
set realname = "Julien Rische"
set mbox_type = Maildir
set mbox = "+INBOX"
set postponed = "+Drafts"
set sendmail = "/usr/bin/msmtp -a julien.rische@cern.ch"
set folder = "~/mail/julien.rische@cern.ch"
set spoolfile = "+INBOX"
set header_cache = "~/.cache/mutt/julien.rische@cern.ch/headers"
set message_cachedir = "~/.cache/mutt/julien.rische@cern.ch/bodies"
set certificate_file = "/etc/ssl/certs/ca-bundle.trust.crt"
set ssl_starttls = yes
set ssl_force_tls = yes

mailboxes +INBOX
