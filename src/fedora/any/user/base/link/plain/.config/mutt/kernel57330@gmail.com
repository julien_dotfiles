set from = "kernel57330@gmail.com"
set realname = "Julien Rische"
set mbox_type = Maildir
set mbox = "+[Gmail].Tous les messages"
set postponed = "+[Gmail].Brouillons"
set sendmail = "/usr/bin/msmtp -a kernel57330@gmail.com"
set folder = "~/mail/kernel57330@gmail.com"
set spoolfile = "+INBOX"
set header_cache = "~/.cache/mutt/kernel57330@gmail.com/headers"
set message_cachedir = "~/.cache/mutt/kernel57330@gmail.com/bodies"
set certificate_file = "/etc/ssl/certs/ca-bundle.trust.crt"
set ssl_starttls = yes
set ssl_force_tls = yes

mailboxes +INBOX
