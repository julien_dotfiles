set from = "julien.rische@gmail.com"
set realname = "Julien Rische"
set mbox_type = Maildir
set mbox = "+[Gmail].Tous les messages"
set postponed = "+[Gmail].Brouillons"
set sendmail = "/usr/bin/msmtp -a julien.rische@gmail.com"
set folder = "~/mail/julien.rische@gmail.com"
set spoolfile = "+INBOX"
set header_cache = "~/.cache/mutt/julien.rische@gmail.com/headers"
set message_cachedir = "~/.cache/mutt/julien.rische@gmail.com/bodies"
set certificate_file = "/etc/ssl/certs/ca-bundle.trust.crt"
set ssl_starttls = yes
set ssl_force_tls = yes

mailboxes +INBOX
