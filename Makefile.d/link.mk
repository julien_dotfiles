ifeq ($(AUTHORITY),root)
  LINK_ORIGIN_PREFIX = /
else
  LINK_ORIGIN_PREFIX = $(HOME)/
endif

REL_PROXY = $(CACHE)/proxy
REL_AUTH_PROXY = $(REL_PROXY)/$(AUTHORITY)

PLINK_REL_TARGET_PATHS = $(foreach path,$(PLINK_PATHS),$(shell $(UTIL)/find_files $(path)))
PLINK_TRUNC_TARGET_PATHS = $(shell $(UTIL)/remove_plain_link_prefix $(SRC_PATH) $(PLINK_REL_TARGET_PATHS))
PLINK_REL_PROXY_PATHS = $(PLINK_TRUNC_TARGET_PATHS:%=$(REL_AUTH_PROXY)/%)
PLINK_ORIGIN_PATHS = $(PLINK_TRUNC_TARGET_PATHS:%=$(LINK_ORIGIN_PREFIX)%)
PLINK_STATUS_RULES = $(PLINK_ORIGIN_PATHS:%=status-link-%)
PLINK_CLEAN_RULES = $(PLINK_ORIGIN_PATHS:%=clean-link-%)

ELINK_REL_TARGET_PATHS = $(foreach path,$(ELINK_PATHS),$(shell $(UTIL)/find_files $(path)))
ELINK_TRUNC_TARGET_PATHS = $(shell $(UTIL)/remove_encrypted_link_prefix $(SRC_PATH) $(ELINK_REL_TARGET_PATHS))
ELINK_REL_PROXY_PATHS = $(ELINK_TRUNC_TARGET_PATHS:%=$(REL_AUTH_PROXY)/%)
ELINK_ORIGIN_PATHS = $(ELINK_TRUNC_TARGET_PATHS:%=$(LINK_ORIGIN_PREFIX)%)
ELINK_STATUS_RULES = $(ELINK_ORIGIN_PATHS:%=status-link-%)
ELINK_CLEAN_RULES = $(ELINK_ORIGIN_PATHS:%=clean-link-%)
ELINK_ENCRYPT_RULES = $(ELINK_REL_TARGET_PATHS:%=encrypt-link-%)

LINK_ORIGIN_PATHS = $(PLINK_ORIGIN_PATHS) $(ELINK_ORIGIN_PATHS)
LINK_STATUS_RULES = $(PLINK_STATUS_RULES) $(ELINK_STATUS_RULES)
LINK_CLEAN_RULES = $(PLINK_CLEAN_RULES) $(ELINK_CLEAN_RULES)

link: $(LINK_ORIGIN_PATHS)
$(LINK_ORIGIN_PATHS): $(LINK_ORIGIN_PREFIX)%: $(REL_AUTH_PROXY)/%
	@$(MKDIR) -p $(dir $@)
	ln -fs $(CURDIR)/$< $@
#	cp -R --preserve=links $< $@

proxy-link: $(LINK_REL_PROXY_PATHS) $(ELINK_REL_PROXY_PATHS)

$(PLINK_REL_PROXY_PATHS): $(REL_AUTH_PROXY)/%:
	@$(MKDIR) -p $(dir $@)
	ln -fs $(CURDIR)/$(filter %$*,$(PLINK_REL_TARGET_PATHS)) $@

$(ELINK_REL_PROXY_PATHS): $(REL_AUTH_PROXY)/%:
	@$(MKDIR) -p $(dir $@)
	$(GPG2_D) < $(filter %$*,$(ELINK_REL_TARGET_PATHS)) > $@

encrypt-link: $(ELINK_ENCRYPT_RULES)
$(ELINK_ENCRYPT_RULES): encrypt-link-%:
	$(GPG2_E) < $(REL_AUTH_PROXY)/$(shell $(UTIL)/remove_encrypted_link_prefix $(SRC_PATH) $*) > $*

status-link: $(LINK_STATUS_RULES)
$(LINK_STATUS_RULES): status-link-$(LINK_ORIGIN_PREFIX)%:
	@if [ -L "$(LINK_ORIGIN_PREFIX)$*" ]; \
		then if file "$(LINK_ORIGIN_PREFIX)$*" | egrep -q "symbolic link to $(CURDIR)/$(REL_AUTH_PROXY)/$*"; \
			then $(ECHO) "[0;32m[X][0m $(LINK_ORIGIN_PREFIX)$*"; \
			else $(ECHO) "[1;31m[ ][0m $(LINK_ORIGIN_PREFIX)$* (invalid link)"; fi\
		elif [ -f "$(LINK_ORIGIN_PREFIX)$*" ]; then $(ECHO) "[1;31m[ ][0m $(LINK_ORIGIN_PREFIX)$* (is a file)"; \
		elif [ -d "$(LINK_ORIGIN_PREFIX)$*" ]; then $(ECHO) "[1;31m[ ][0m $(LINK_ORIGIN_PREFIX)$* (is a directory)"; \
		else $(ECHO) "[0;33m[-][0m $(LINK_ORIGIN_PREFIX)$* (file doesn't exist)"; fi

clean-link: $(LINK_CLEAN_RULES)
$(LINK_CLEAN_RULES): clean-link-$(LINK_ORIGIN_PREFIX)%:
	@if [ -L "$(LINK_ORIGIN_PREFIX)$*" ]; then \
		if file "$(LINK_ORIGIN_PREFIX)$*" | grep -q "symbolic link to $(CURDIR)/$(REL_AUTH_PROXY)/$*"; then \
			$(ECHO) $(RM) $(LINK_ORIGIN_PREFIX)$*; \
			$(RM) $(LINK_ORIGIN_PREFIX)$*; \
		fi \
	fi
