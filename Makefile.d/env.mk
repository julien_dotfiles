ECHO = echo
MKDIR = /bin/mkdir
TOUCH = /bin/touch
GPG2_E = /usr/bin/gpg2 --batch -q -r 'Julien Rische' -e
GPG2_D = /usr/bin/gpg2 --batch -q -u 'Julien Rische' -d

CACHE = $(PREFIX)cache
SRC_PATH = $(PREFIX)src

ifeq ($(USER),root)
  AUTHORITY = root
else
  AUTHORITY = user
endif

ifeq ($(DISTRO_SPECIFIC),1)
  DISTRO_PATHS = $(SRC_PATH)/$(DISTRO)
else
  DISTRO_PATHS = $(SRC_PATH)/any $(SRC_PATH)/$(DISTRO)
endif

ifeq ($(GPU),none)
  GPU_PATHS = $(DISTRO_PATHS:%=%/any)
else
  GPU_PATHS = $(DISTRO_PATHS:%=%/any) $(DISTRO_PATHS:%=%/$(GPU))
endif

AUTHORITY_PATHS = $(GPU_PATHS:%=%/$(AUTHORITY))

ifeq ($(TARGET),base)
  TARGETS = base
else
  TARGETS = base $(TARGET)
endif
ifeq ($(TARGET),all)
  TARGETS = base devel graphical
endif

TARGET_PATHS = $(foreach x,$(AUTHORITY_PATHS),$(foreach y,$(TARGETS),$(x)/$(y)))

ifeq ($(AUTHORITY),root)
  PKGS_PATHS = $(TARGET_PATHS:%=%/packages)
$(PKGS_PATHS): %:
	$(MKDIR) -p $(dir $@)
	$(TOUCH) $@
  GIT_REPOS_DIR = /root/code/build
else
  GIT_REPOS_DIR = $(HOME)/code/build
endif

PLINK_PATHS = $(TARGET_PATHS:%=%/link/plain)
$(PLINK_PATHS): %:
	$(MKDIR) -p $@

ELINK_PATHS = $(TARGET_PATHS:%=%/link/encrypted)
$(ELINK_PATHS): %:
	$(MKDIR) -p $@

SCRIPTS_DIR_PATHS = $(TARGET_PATHS:%=%/scripts)
$(SCRIPTS_DIR_PATHS): %:
	$(MKDIR) -p $@

SCRIPTS_PATHS = $(foreach path,$(SCRIPTS_DIR_PATHS),$(wildcard $(path)/*))

GIT_PATHS = $(TARGET_PATHS:%=%/git)
$(GIT_PATHS): %:
	$(MKDIR) -p $(dir $@)
	$(TOUCH) $@

FINAL_PATHS = $(PKGS_PATHS) $(PLINK_PATHS) $(ELINK_PATHS) $(SCRIPTS_DIR_PATHS) $(GIT_PATHS)
