PWDIR_NAME = .password-store
PWDIR_PATH = $(PREFIX)pw/$(AUTHORITY).tar.gz.pgp
PWDIR_CACHE_PATH = $(REL_AUTH_PROXY)/$(PWDIR_NAME)
PWDIR_ORIGIN_PATH = $(LINK_ORIGIN_PREFIX)$(PWDIR_NAME)

ifeq ($(AUTHORITY),user)
import-pw: $(PWDIR_ORIGIN_PATH)

$(PWDIR_CACHE_PATH): $(PWDIR_PATH)
	$(MKDIR) -p $(REL_AUTH_PROXY)
	$(GPG2_D) < $< | tar -xz -C $(REL_AUTH_PROXY)

$(PWDIR_ORIGIN_PATH): $(PWDIR_CACHE_PATH)
	ln -fs $(CURDIR)/$< $@

status-pw:
	@if [ -L "$(PWDIR_ORIGIN_PATH)" ]; then \
		if file "$(PWDIR_ORIGIN_PATH)" | egrep -q "symbolic link to $(CURDIR)/$(PWDIR_CACHE_PATH)"; \
			then $(ECHO) "[0;32m[X][0m $(PWDIR_ORIGIN_PATH)"; \
			else $(ECHO) "[1;31m[ ][0m $(PWDIR_ORIGIN_PATH) (invalid link)"; fi\
		elif [ -f "$(PWDIR_ORIGIN_PATH)" ]; then $(ECHO) "[1;31m[ ][0m $(PWDIR_ORIGIN_PATH) (is a file)"; \
		elif [ -d "$(PWDIR_ORIGIN_PATH)" ]; then $(ECHO) "[1;31m[ ][0m $(PWDIR_ORIGIN_PATH) (is a directory)"; \
		else $(ECHO) "[0;33m[-][0m $(PWDIR_ORIGIN_PATH) (file doesn't exist)"; fi

export-pw:
	tar -cz -C $(REL_AUTH_PROXY) $(PWDIR_NAME) | $(GPG2_E) > $(PWDIR_PATH)
	$(TOUCH) -c $(PWDIR_PATH)

clean-pw:
	@if [ -L "$(PWDIR_ORIGIN_PATH)" ]; then \
		if file "$(PWDIR_ORIGIN_PATH)" | grep -q "symbolic link to $(CURDIR)/$(PWDIR_CACHE_PATH)"; then \
			$(ECHO) $(RM) $(PWDIR_ORIGIN_PATH); \
			$(RM) $(PWDIR_ORIGIN_PATH); \
		fi \
	fi
else
import-pw:
status-pw:
export-pw:
clean-pw:
endif
