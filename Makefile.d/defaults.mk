UTIL = bin

ifndef DISTRO
  DISTRO = $(shell $(UTIL)/distro)
endif

ifndef GPU
  GPU = $(shell $(UTIL)/gpu)
endif

ifndef PREFIX
  PREFIX =
endif

ifndef HOME
  HOME = $(shell 'echo -n "${HOME}"')
endif

ifndef GIT
  GIT = git
endif

ifndef TARGET
  $(error please set TARGET=base|devel|graphical|all)
endif
