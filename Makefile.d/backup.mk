BKP_FILES = \
  $(shell cd; find '.macromedia/Flash_Player/\#SharedObjects' -type d -name '\#localWithNet') \
  $(shell $(UTIL)/files_exist $(HOME) .config/UNDERTALE_linux .config/CSDSteamBuild .config/ppsspp .fceux .minecraft/saves .local/share/CaveStory+)

BKP_ARCHIVE = backup$(shell date '+%Y%m%d%H%M%S').zip

backup:
	cd;	zip $(BKP_ARCHIVE) -r $(BKP_FILES)
