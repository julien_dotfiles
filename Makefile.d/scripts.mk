DO_SCRIPTS_RULES = $(SCRIPTS_PATHS:%=do-%)
UNDO_SCRIPTS_RULES = $(SCRIPTS_PATHS:%=undo-%)
STATUS_SCRIPTS_RULES = $(SCRIPTS_PATHS:%=status-%)
DONE_SCRIPTS_RULES = $(SCRIPTS_PATHS:%=$(CACHE)/%/done)

do-scripts: $(DO_SCRIPTS_RULES)
undo-scripts: $(UNDO_SCRIPTS_RULES)
status-scripts: $(STATUS_SCRIPTS_RULES)

.PHONY: $(DONE_SCRIPTS_RULES)
$(DONE_SCRIPTS_RULES): $(CACHE)/%/done: %/status
	@$(MKDIR) -p $(CACHE)/$*/status
	@touch $(CACHE)/$*/status/out $(CACHE)/$*/status/err
	@$(RM) $@
	@if ./$< > $(CACHE)/$*/status/out 2> $(CACHE)/$*/status/err; then \
		touch $@; \
	fi

$(DO_SCRIPTS_RULES): do-%: %/do $(CACHE)/%/done
	@$(MKDIR) -p $(CACHE)/$*/do
	@touch $(CACHE)/$*/do/out $(CACHE)/$*/do/err
	@if [ ! -f $(CACHE)/$*/done ]; then \
		$(ECHO) 'do $*'; \
		./$< > $(CACHE)/$*/do/out 2> $(CACHE)/$*/do/err; \
	fi

$(UNDO_SCRIPTS_RULES): undo-%: %/undo $(CACHE)/%/done
	@$(MKDIR) -p $(CACHE)/$*/undo
	@touch $(CACHE)/$*/undo/out $(CACHE)/$*/undo/err
	@if [ -f $(CACHE)/$*/done ]; then \
		$(ECHO) 'undo $*'; \
		./$< > $(CACHE)/$*/undo/out 2> $(CACHE)/$*/undo/err; \
	fi

$(STATUS_SCRIPTS_RULES): status-%: $(CACHE)/%/done
	@if [ -f $< ]; then \
		$(ECHO) '[0;32m[X][0m $*'; \
	else \
		$(ECHO) '[0;33m[ ][0m $*'; \
	fi
