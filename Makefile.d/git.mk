GIT_HTTP_REPOS = $(shell egrep -h 'https?://' $(GIT_PATHS) /dev/null | sed 's/https\?:\/\///g')

GIT_HTTP_CLONE_RULES = $(GIT_HTTP_REPOS:%=git-http-clone-%)
GIT_UPDATE_RULES = $(addprefix git-update-,$(notdir $(GIT_HTTP_REPOS)))
GIT_HTTP_STATUS_RULES = $(GIT_HTTP_REPOS:%=git-http-status-%)

git-clone-all: $(GIT_HTTP_CLONE_RULES)
git-update-all: $(GIT_UPDATE_RULES)
status-git: $(GIT_HTTP_STATUS_RULES)

$(GIT_REPOS_DIR):
	$(MKDIR) -p $@

$(GIT_HTTP_CLONE_RULES): git-http-clone-%: $(GIT_REPOS_DIR)
	@if [ ! -d "$(GIT_REPOS_DIR)/$(notdir $*)" ]; then \
		echo '$(GIT) -C $(GIT_REPOS_DIR) clone --recursive "https://$*"'; \
		$(GIT) -C $(GIT_REPOS_DIR) clone --recursive "https://$*"; fi

$(GIT_UPDATE_RULES): git-update-%: $(GIT_REPOS_DIR)/%
	$(GIT) -C $< pull
	$(GIT) -C $< submodule update

$(GIT_HTTP_STATUS_RULES): git-http-status-%:
	@if [ -d "$(GIT_REPOS_DIR)/$(notdir $*)" ]; then \
		$(GIT) -C "$(GIT_REPOS_DIR)/$(notdir $*)" remote update > /dev/null 2>&1; \
		if LANG=en_US.UTF-8 $(GIT) -C "$(GIT_REPOS_DIR)/$(notdir $*)" status -uno | grep -q 'Your branch is up-to-date'; \
			then echo "\033[32m[X]\033[0m https://$* -> $(GIT_REPOS_DIR)/$(notdir $*)"; \
			else echo "\033[33m[-]\033[0m https://$* -> $(GIT_REPOS_DIR)/$(notdir $*) (out-of-date)"; fi; \
	else echo "\033[1;31m[ ]\033[0m https://$* -> $(GIT_REPOS_DIR)/$(notdir $*)"; fi
