PKGS = $(shell cat $(PKGS_PATHS) /dev/null)

PKG_STATUS_RULES = $(PKGS:%=status-pkg-%)

status-pkgs: $(PKG_STATUS_RULES)
$(PKG_STATUS_RULES): status-pkg-%:
	@if $(PKG_MNG_TEST) $* > /dev/null 2>&1; \
		then $(ECHO) "[0;32m[X][0m $*"; \
		else $(ECHO) "[1;31m[ ][0m $*"; fi

ifeq ($(PKGS),)
install-pkgs:
else
install-pkgs:
	@if ! $(PKG_MNG_TEST) $(PKGS) > /dev/null 2>&1; then \
		$(ECHO) install $(PKGS); \
		$(PKG_MNG_INSTALL) $(PKGS); fi
endif
