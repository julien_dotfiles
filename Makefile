status: status-link status-scripts status-git status-pkgs status-pw
all: link do-scripts install-pkgs import-pw
clean: clean-link clean-pw

include config.mk

include Makefile.d/arch.mk
include Makefile.d/debian.mk
include Makefile.d/voidlinux.mk
include Makefile.d/fedora.mk
include Makefile.d/defaults.mk
include Makefile.d/env.mk
include Makefile.d/distro.mk
include Makefile.d/scripts.mk
include Makefile.d/link.mk
include Makefile.d/pass.mk
include Makefile.d/packages.mk
include Makefile.d/git.mk
include Makefile.d/backup.mk
include Makefile.d/debug.mk

prepare-git-commit: encrypt-link export-pw
	find $(SRC_PATH) -printf "git add %p\n" -exec git add "{}" \;

bootstrap: $(FINAL_PATHS)
	git add $^
ifeq ($(AUTHORITY),user)
	$(MAKE) USER=root bootstrap
endif
